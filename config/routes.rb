Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get '/' => 'index#index'
  get '/games' => 'video_games#index'
  get '/games/new' => 'video_games#new'
  post '/games' => 'video_games#create'
  get '/games/:slug/details' => 'video_games#show'
end

class VideoGame < ActiveRecord::Base
  has_many :releases
  has_many :platforms, through: :releases
  has_and_belongs_to_many :genres
  belongs_to :developer
  before_create :ensure_slug_exists
  def add_platform(platform_id)

  end

  private

  def ensure_slug_exists
    return unless !self.title.nil? && !self.title.empty?
    self.slug = self.title.slugify unless !self.slug.nil? && !self.slug.empty?
  end
end

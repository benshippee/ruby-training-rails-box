class Genre < ActiveRecord::Base
  has_and_belongs_to_many :video_games
  before_create :ensure_slug_exists

  private

  def ensure_slug_exists
    return unless !self.name.nil? && !self.name.empty?
    self.slug = self.name.slugify unless !self.slug.nil? && !self.slug.empty?
  end
end

class PlatformsController < ApplicationController
  before_action :set_platform, :only => [:show, :update, :destroy]

  def new
    @platform = Platform.new
  end

  def create
    @platform = Platform.new(platform_params)
  end

  def show
    @releases = @platform.releases
    @developer = @platform.developer
  end

  private

  def set_platform
    @platform = Platform.where(slug: params[:slug]).first
  end

  def platform_params
    params.require(:platform).permit(:name, :developer_id, :release_year, :description)
  end
end

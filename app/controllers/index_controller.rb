class IndexController < ApplicationController
  def index
    @recent_game_adds = VideoGame.last(10)
    @recent_platform_adds = Platform.last(10)
  end
end

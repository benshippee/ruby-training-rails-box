class VideoGamesController < ApplicationController
  before_action :set_video_game, :only => [:show, :update, :destroy]

  def index
    @video_games = VideoGame.all
  end

  def new
    @video_game = VideoGame.new
  end

  def create
    @video_game = VideoGame.create(video_game_params)
    redirect_to 'video_games#index'
  end

  def show
    @releases = @video_game.releases
    @developer = @video_game.developer
  end

  private

  def set_video_game
    @video_game = VideoGame.where(slug: params[:slug]).first
  end

  def video_game_params
    params.require(:video_game).permit(:title, :players, :developer_id, :release_year, :description)
  end
end

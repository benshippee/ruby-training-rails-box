class CreateGenres < ActiveRecord::Migration
  def change
    create_table :genres do |t|
      t.string :name, null: false
      t.text :description
      t.timestamps null: false
    end

    create_table :video_game_genres, id: false do |t|
      t.belongs_to :video_game, index: true
      t.belongs_to :genre, index: true
    end
  end
end

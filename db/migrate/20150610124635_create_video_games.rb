class CreateVideoGames < ActiveRecord::Migration
  def change
    create_table :video_games do |t|
      t.string :title, null: false
      t.string :slug, null: false, unique: true
      t.integer :players
      t.integer :developer_id
      t.integer :release_year
      t.text :description
      t.timestamps null: false
    end
  end
end

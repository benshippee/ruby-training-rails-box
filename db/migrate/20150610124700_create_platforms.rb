class CreatePlatforms < ActiveRecord::Migration
  def change
    create_table :platforms do |t|
      t.string :name, null: false
      t.string :slug, null: false
      t.text :description
      t.integer :release_year
      t.integer :developer_id
      t.timestamps null: false
    end
  end
end

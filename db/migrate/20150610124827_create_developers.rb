class CreateDevelopers < ActiveRecord::Migration
  def change
    create_table :developers do |t|
      t.string :name, null: false
      t.text :description
      t.string :slug, null: false
      t.timestamps null: false
    end

    create_table :video_game_developers, id: false do |t|
      t.belongs_to :video_game, index: true
      t.belongs_to :developer, index: true
    end
  end
end
